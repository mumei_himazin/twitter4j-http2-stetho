twitter4j-http2-Stetho
---------------------

 Twitter4JでStethoを使えるようにしてみた。
 
 参考 http://qiita.com/takke/items/96b7a45f0476d546b5b1  
 参考 http://qiita.com/wasabeef_jp/items/1e7bdb78766708206d3d  

###導入###
```java
repositories {
    mavenCentral()
	maven{
		url 'http://repository.mumei-himazin.info/nexus/content/repositories/releases/'
	}
}
dependencies {
	compile 'org.twitter4j:twitter4j-core:4.0.3'
	compile 'org.twitter4j:twitter4j-http2-support:4.0.3'
	compile 'info.mumei-himazin:t4j-h2-stetho:2.0'
}
```


###使い方###
```java
Twitter twitter = new TwitterFactory(configuration).getInstance();
twitter4j.stetho.Twitter4jStetho.initialize(twitter);
```
後は参考のところと同じようにApplicationで  

```java
public class Application extends android.app.Application{
	@Override
	public void onCreate() {
		super.onCreate();
		com.facebook.stetho.Stetho.initialize(
			com.facebook.stetho.Stetho.newInitializerBuilder(this)
				.enableDumpapp(com.facebook.stetho.Stetho.defaultDumperPluginsProvider(this))
				.enableWebKitInspector(com.facebook.stetho.Stetho.defaultInspectorModulesProvider(this))
				.build()
		);
	}
}
```



###Licence###
```
Copyright (C) 2015 @mumei_himazin https://twitter.com/mumei_himazin
	 
 Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at  
  
     http://www.apache.org/licenses/LICENSE-2.0  
  
Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.
```