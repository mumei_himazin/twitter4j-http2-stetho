/*
 * Copyright (c) 2015. Yuuto Uehara  mumei.himazin at gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package twitter4j.stetho;



import com.facebook.stetho.okhttp3.StethoInterceptor;
import okhttp3.OkHttpClient;
import twitter4j.AlternativeHttpClientImpl;
import twitter4j.HttpClient;
import twitter4j.HttpClientFactory;
import twitter4j.Twitter;
import twitter4j.conf.Configuration;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Twitter4jStetho {

	private Twitter4jStetho(){}


	public static void initialize(Twitter twitter){
		checkHttp2Support();

		Configuration configuration = twitter.getConfiguration();
		HttpClient httpClient = HttpClientFactory.getInstance(configuration.getHttpClientConfiguration());

		if(!(httpClient instanceof AlternativeHttpClientImpl)){
			throw new IllegalStateException("twitter4j-http2-support not use.");
		}
		AlternativeHttpClientImpl http2Client = (AlternativeHttpClientImpl)httpClient;

		try{
			Method method = http2Client.getClass().getDeclaredMethod("prepareOkHttpClient");
			method.setAccessible(true);
			method.invoke(http2Client);

			Field f = http2Client.getClass().getDeclaredField("okHttpClient");
			f.setAccessible(true);
			OkHttpClient okHttpClient = (OkHttpClient)f.get(http2Client);
			OkHttpClient.Builder builder = okHttpClient.newBuilder();
			builder.networkInterceptors().add(new StethoInterceptor());
			f.set(http2Client,builder.build());
		} catch (NoSuchMethodException ignore) {
		} catch (InvocationTargetException  ignore) {
		} catch (IllegalAccessException  ignore) {
		} catch (NoSuchFieldException  ignore) {
		}
	}

	private static void checkHttp2Support(){
		try {
			Class.forName("twitter4j.AlternativeHttpClientImpl");
			Class.forName("twitter4j.OkHttpResponse");
			Class.forName("twitter4j.VersionHTTP2");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("twitter4j-http2-support not use.");
		}
	}


}
